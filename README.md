Sorting App Project Readme
This document provides an overview of the Sorting App project, including its functionality, testing methods, and build instructions.

Project: Sorting App

Description:

This Java application takes user input of 10 integers, sorts them in ascending order, and prints the sorted list to the console. The program also handles invalid inputs (like non-numeric values) by replacing them with zeros.

Components:

SortingApp.java: This class contains the main logic of the application, handling user input, sorting, and output.
AppTest.java: This class contains unit tests for the SortingApp using JUnit. These tests verify its behavior for various input scenarios.
ParameterizedTests.java: This class utilizes parameterized tests with JUnit to test the application using data from a separate file (variable-inputs.txt).
pom.xml: This file defines the project's configuration for building and managing dependencies using Maven.
Testing:

The project utilizes two testing approaches:

Unit Tests: The AppTest.java class provides unit tests that directly test the functionality of the SortingApp class in isolation.
Parameterized Tests: The ParameterizedTests.java class leverages parameterized tests with JUnit. This allows running the same test with different input and expected output data stored in a separate file, making testing more flexible and scalable.
Building and Running:

To build and run the application:

Ensure you have Maven installed on your system.
Open a terminal in the project directory.
Run mvn clean install to compile the project and generate the executable JAR file.
Run java -jar target/sorting-app-project-1.0.jar to execute the application.
Note: You can modify the input provided to the program through the console or by replacing the content of variable-inputs.txt and running the parameterized tests again.