package com.example;

import java.util.Arrays;
import java.util.Scanner;


public class SortingApp 
{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[10];

       for (int i = 0; i < 10; i++) {
            try {
                String input = scanner.nextLine();
                long num = Long.parseLong(input);
                if (num > Integer.MAX_VALUE|| num < Integer.MIN_VALUE) {
                    numbers[i] = 0;
                } else {
                    numbers[i] = (int) num;
                }
            } catch (NumberFormatException nfe) {
                numbers[i] = 0; 
            }
        }
        Arrays.sort(numbers);

        for (int numero : numbers) {
            System.out.print(numero + " ");
        }
        scanner.close();
    }
}

