package com.example;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.apache.commons.io.IOUtils;

@RunWith(Parameterized.class)
public class ParameterizedTests {
    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final InputStream originalIn = System.in;
    private String input;
    private String expected;

    public ParameterizedTests(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outputStream));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setIn(originalIn);
    }

    @Test
    public void testParamerizedInputs() {

        System.setIn(new ByteArrayInputStream(input.getBytes()));
        SortingApp.main(new String[] {});
        String actualOutput = outputStream.toString().trim();

        assertEquals(expected, actualOutput);
    }

    @Parameters
    public static Collection<Object[]> data() {
        Collection<Object[]> testData = new ArrayList<>();
        try {
            FileInputStream inputStream = new FileInputStream(new File("./Resources/variable-inputs.txt"));
            String content = IOUtils.toString(inputStream, "UTF-8");
            String[] lines = content.split(";");

            for (String line : lines) {
                String[] separatedLine = line.split(":");
                Object[] inputAndExpected = new Object[] { separatedLine[0], separatedLine[1] };
                testData.add(inputAndExpected);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return testData;
    }
}
