package com.example;

import static org.junit.Assert.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for SortingApp.
 */
public class AppTest{
    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final InputStream originalIn = System.in;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outputStream));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setIn(originalIn);
    }

    @Test
    public void testSameInputNumbers() {
        // Simular entrada del usuario
        String input = "5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n";
        System.setIn(new ByteArrayInputStream(input.getBytes()));

        // Ejecutar el programa
        SortingApp.main(new String[]{});

        // Obtener la salida del programa
        String expectedOutput = "5 5 5 5 5 5 5 5 5 5";
        String actualOutput = outputStream.toString().trim();

        assertEquals(expectedOutput, actualOutput);
    }
    @Test
    public void testZeroInputs() {
        // Simular entrada del usuario
        String input = "\n\n\n\n\n\n\n\n\n\n";
        System.setIn(new ByteArrayInputStream(input.getBytes()));

        // Ejecutar el programa
        SortingApp.main(new String[]{});

        // Obtener la salida del programa
        String expectedOutput = "0 0 0 0 0 0 0 0 0 0";
        String actualOutput = outputStream.toString().trim();

        assertEquals(expectedOutput, actualOutput);
    }
    @Test
    public void testOneInput() {
        // Simular entrada del usuario
        String input = "\n\n\n\n\n1\n\n\n\n\n";
        System.setIn(new ByteArrayInputStream(input.getBytes()));

        // Ejecutar el programa
        SortingApp.main(new String[]{});

        // Obtener la salida del programa
        String expectedOutput = "0 0 0 0 0 0 0 0 0 1";
        String actualOutput = outputStream.toString().trim();

        assertEquals(expectedOutput, actualOutput);
    }
    @Test
    public void testOverflowIntMaxValue() {
        // Simular entrada del usuario
        String input = "2147483647\n2147483648\n2147483646\n-2147483648\n2147483645\n-2147483649\n-2147483647\n2147483653\n2147483652\n2147483640\n";
        System.setIn(new ByteArrayInputStream(input.getBytes()));

        // Ejecutar el programa
        SortingApp.main(new String[]{});

        // Obtener la salida del programa
        String expectedOutput = "-2147483648 -2147483647 0 0 0 0 2147483640 2147483645 2147483646 2147483647";
        String actualOutput = outputStream.toString().trim();

        assertEquals(expectedOutput, actualOutput);
    }
    

}
